const ch = require('child_process');

const WebSocket = require('ws');

let script = ch.spawn('./script', ['-f', '-e', '/dev/null'], { detached: true });

script.stdout.pipe(process.stdout);
process.stdin.setRawMode(true);
process.stdin.pipe(script.stdin);
process.stdin.on('keypress', (a,d) => {
	console.log('a',a,d);
});

script.on('exit', (code, signal) => process.exit());
process.on('exit', () => console.log('exit'));
process.on('SIGTERM', () => {});
process.on('SIGINT', () => {});

const port = process.argv.pop();
console.log(port);

const wss = new WebSocket.Server({
	host: '0.0.0.0',
	port: port || 2020,
});

wss.on('connection', (ws) => {
	console.log('connected');
	ws.on('close', () => {
		console.log('closed');
		//script.stdout.off('data', handleStream);
		script.stdout.unpipe(duplex);
	});
	ws.on('error', (e) => {
		console.log('ws error');
		console.error(e);
	});

	const duplex = WebSocket.createWebSocketStream(ws, { encoding: 'utf8' });
	duplex.pipe(script.stdin);
	script.stdout.pipe(duplex);
	//script.stdout.on('data', handleStream);
});

wss.on('error', (e) => {
	console.log('wss error');
	console.error(e);
});